const fs = require('fs')
const path = require('path')

const minutesToTime = (raw) => {
  const hours = Math.floor(raw / 60)
  const minutes = (raw % 60)
  return `${hours}:${minutes || "00"}`
}

exports.handler = async function(event, context) {
  const db = JSON.parse(fs.readFileSync(path.join(__dirname, '..', '..', 'db.json')))
  const now = new Date()
  const nowTime = now.getHours() * 60 + now.getMinutes()
  const opened = db[new Date().getDay()]
  const response = opened.from < nowTime && opened.to > nowTime
    ? `Vietnamec otvoreny! (${minutesToTime(opened.from)} - ${minutesToTime(opened.to)})`
    : `Vietnamec zatvoreny! (${minutesToTime(opened.from)} - ${minutesToTime(opened.to)})`
  return {
    statusCode: 200,
    body: response
  }
}
